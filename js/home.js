window.addEventListener('click', (e) => {
    if (getE('my_btn_select_loc').contains(e.target)) {
        getE('my_select_loc').style.display = 'block'
    } else {
        getE('my_select_loc').style.display = 'none'
    }
})

window.addEventListener('click', (e) => {
    if (getE('h_lg_button').contains(e.target)) {
        getE('lg_menu').style.visibility = 'visible'
        getE('lg_menu').style.opacity = '1'
    } else {
        getE('lg_menu').style.visibility = 'hidden'
        getE('lg_menu').style.opacity = '0'
    }
    if (getE('h_lg_button').contains(e.target)) {
        getE('lg_menu_show').style.visibility = 'visible'
        getE('lg_menu_show').style.opacity = '1'
        getE('lg_menu_show').style.transform = 'translate(0px)'
    } else {
        getE('lg_menu_show').style.visibility = 'hidden'
        getE('lg_menu_show').style.opacity = '0'
        getE('lg_menu_show').style.transform = 'translate(150px)'
    }
    if (getE('btn_lg_menu_show_loc').contains(e.target)) {
        getE('lg_menu_show_loc').style.display = 'block'
        getE('lg_menu').style.visibility = 'visible'
        getE('lg_menu').style.opacity = '1'
        getE('lg_menu_show').style.visibility = 'visible'
        getE('lg_menu_show').style.opacity = '1'
        getE('lg_menu_show').style.transform = 'translate(0px)'
        getE('lg_menu_show_loc_bg').style.visibility = 'visible'
        getE('lg_menu_show_loc_bg').style.opacity = '1'
    } else {
        getE('lg_menu_show_loc').style.display = 'none'
        getE('lg_menu_show_loc_bg').style.visibility = 'hidden'
        getE('lg_menu_show_loc_bg').style.opacity = '0'
    }
    let li = document.querySelectorAll('#lg_menu_show_loc ul li')
    li.forEach(element => {
        if (element.contains(e.target)) {
            getE('lg_menu').style.visibility = 'visible'
            getE('lg_menu').style.opacity = '1'
            getE('lg_menu_show').style.visibility = 'visible'
            getE('lg_menu_show').style.opacity = '1'
            getE('lg_menu_show').style.transform = 'translate(0px)'
            return
        }
    });
})

let sticker = document.querySelectorAll('.film_item_name_sticker');
sticker.forEach(element => {
    if (element.innerHTML == 'P') {
        element.style.backgroundColor = '#00ac4d';
        element.style.padding = '2px 14px';
    } else {
        element.style.backgroundColor = '#fb4226';
        
    }
});

$('#my_carousel_banner').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    arrows: true
});

$('.my_carousel_film').slick({
    arrows: true
});